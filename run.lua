local f = ...
local settings = require 'settings'

if not f then
	f = settings.path
end

local csv = require 'ftcsv'

local round = function (num, numDecimalPlaces)
	local mult = 10^(numDecimalPlaces or 0)
	return math.floor(num * mult + 0.5) / mult
end

local mean = function (t)
	local sum = 0
	for _,v in ipairs(t) do
		sum = sum + v
	end
	return (sum / #t)
end

local standardDeviation = function (t)
	local m = mean(t)
	local vm
	local sum = 0
	local count = 0
	local result

	for k,v in pairs(t) do
		if type(v) == 'number' then
			vm = v - m
			sum = sum + (vm * vm)
			count = count + 1
		end
	end

	result = math.sqrt(sum / (count-1))
	return result
end

local data,headers = csv.parse(f,',')

local dungeons = {
	"Giant's Keep B12",
	"Dragon's Lair B12",
	"Necropolis B12",
	"Steel Fortress B10",
	"Punisher's Crypt B10"
}

local drops = {
	'Rune Piece',
	'Symbol of Harmony',
	'Symbol of Transcendence',
	'Symbol of Chaos',
	'Unknown Scroll',
	'Mystical Scroll',
	'Rare Rune',
	'Hero Rune',
	'Legendary Rune',
	'Rare Artifact',
	'Hero Artifact',
	'Legendary Artifact',
}

-- Set up tracking tables
local interesting = {}
for _,v in ipairs(dungeons) do
	interesting[v] = {total = 0, teams = {}, drops = {}}
	for _,drop in ipairs(drops) do
		interesting[v].drops[drop] = 0
	end
end

-- Time to parse!
local interestingRuns = 0
for i,run in ipairs(data) do
	if interesting[run.dungeon] then
		-- Collect win/loss data
		interestingRuns = interestingRuns + 1
		local d = interesting[run.dungeon]
		d.total = d.total + 1
		d[run.result] = (d[run.result] or 0) + 1
		
		-- Store teams and successful runs + averages by team
		local team = run.team1 .. ' (L), ' .. run.team2 .. ', ' .. run.team3 .. ', ' .. run.team4 .. ', ' .. run.team5
		local minutes, seconds = string.match(run.time, ('(%d*):(%d*)'))
		local time
		if minutes and seconds and run.result == 'Win' then
			minutes = tonumber(minutes)
			seconds = tonumber(seconds)
			time = (minutes * 60) + seconds
		end
		
		-- Store team data
		d.teams[team] = d.teams[team] or {total = 0, times = {}}
		local t = d.teams[team]
		t.total = t.total + 1
		t[run.result] = (t[run.result] or 0) + 1
		if time then
			table.insert(t.times,time)
		end

		-- Store drop stats
		local drop = run.drop
		if drop == 'Rune' and run.grade == '6*' then
			if run.rarity == 'Legendary' then
				d.drops['Legendary Rune'] = d.drops['Legendary Rune'] + 1
			end
			if (run.rarity == 'Hero') and settings.showHero then
				d.drops['Hero Rune'] = d.drops['Hero Rune'] + 1
			end
			if (run.rarity == 'Rare') and settings.showRare then
				d.drops['Rare Rune'] = d.drops['Rare Rune'] + 1
			end
		elseif drop == 'Artifact' then
			if run.rarity == 'Legendary' then
				d.drops['Legendary Artifact'] = d.drops['Legendary Artifact'] + 1
			end
			if (run.rarity == 'Hero') and settings.showHero then
				d.drops['Hero Artifact'] = d.drops['Hero Artifact'] + 1
			end
			if (run.rarity == 'Rare') and settings.showRare then
				d.drops['Rare Artifact'] = d.drops['Rare Artifact'] + 1
			end
		elseif d.drops[run.drop] then
			d.drops[run.drop] = d.drops[run.drop] + 1
		elseif run.drop then
			local item,quantity = string.match(run.drop,'(.-) x(%d+)')
			if item and quantity then
				if d.drops[item] then
					quantity = tonumber(quantity)
					d.drops[item] = d.drops[item] + quantity
				end
			end
		end
	end
end

-- Report all the things
for _,dungeon in ipairs(dungeons) do
	local i = interesting[dungeon]
	if i.Win or i.Lost then
		print("Dungeon: " .. dungeon)
		print("  Success rate: " .. round(((i.Win or 0) / i.total) * 100, 2) .. "% over " .. i.total .. " runs" )
		print(' ')
		-- Report interesting drop stats
		for _,drop in ipairs(drops) do
			if i.drops[drop] > 0 then print('  *' .. drop .. ': ' .. i.drops[drop] .. ' (' .. round((i.drops[drop] / i.Win),2) .. ' per run)') end
		end
		
		print(' ')
		-- Report team data
		for team, results in pairs(i.teams) do
			local t = i.teams[team]
			print("    Team: " .. team)
			print("    --Success rate: " .. round(((t.Win or 0) / t.total) * 100, 2) .. "% over " .. t.total .. " runs")
			local sum  = 0
			local high = 0
			local low = math.huge
			for _,time in ipairs(t.times) do
				sum = sum + time
				high = math.max(high,time)
				low = math.min(low,time)
			end
			local avg = round(sum / #t.times, 1)
			print("    --Average time: " .. avg .. " seconds." .. (#t.times > 1 and " Standard deviation: " .. round(standardDeviation(t.times),1) or ''))
			print("    --Fastest: " .. low .. " seconds. Slowest: " .. high .. " seconds.")
			print(" ")
		end
		print("\n\n")
	end
end

io.write"Press Enter to continue...";io.read()
