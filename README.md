# Summoners War Run Scraper #

This utility takes .csv files created via [SWEX](https://github.com/Xzandro/sw-exporter) with the run logger plugin enabled and gives you a readout of what teams you used, their success rates, and their average times.

Currently it only watches for B12 dungeons, PB10, and SB10.

### Requirements ###
* [Lua 5.3+](http://luabinaries.sourceforge.net/download.html)
* [Latest Summoners War Exporter](https://github.com/Xzandro/sw-exporter/releases)

### How to use (Windows) ###
* Enable the RunLogger plugin in SWEX, along with the Log Wipes option. Run a few dungeons and you should see a .csv file in your SWEX export directory (usually `Summoners War Exporter Files` on your desktop, otherwise check SWEX settings).
* Unzip Lua binaries to the directory where you want the scraper to live (probably in your SWEX export directory).
* Copy `ftcsv.lua`, `run.lua`, and optionally `settings.lua` to the same location.
* Copy the full path of your .csv file into the bracketed section of `settings.lua`. An example is in there for you to follow.
* Rightclick `run.lua`, select `Open with`, `Choose another app`, more apps, look for another app on this PC. Find your Lua executables and select either `Lua53.exe` or `Lua54.exe` and tell Windows to always open Lua files with that program.
* Doubleclick `run.lua` and enjoy your delicious data!

### How to use (Mac/Linux) ###
* See first step above.
* Install Lua 5.3+ as per your package manager.
* Copy `ftcsv.lua`, `run.lua`, and optionally `settings.lua` to your SWEX export directory.
* Run with `lua run.lua your_csv_file.csv`. Optionally you can specify your csv file by path in `settings.lua`.
